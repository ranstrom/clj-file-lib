(ns clj-file-lib.formats.csv
  (:require [clj-file-lib.parse :as p]
            [clj-file-lib.formats.gzip :as gzip]
            [clojure.data.csv :as csv]))

(def batch-size-write 100000)

(defn write-to-file
  [{:keys [output-filepath gzip include-header separator-char header write-size]} data]
  (with-open [writer (gzip/get-writer output-filepath gzip)]
    (when include-header
      (csv/write-csv writer [header] :separator separator-char))
    (doseq [batch (partition-all (or write-size batch-size-write) data)]
      (csv/write-csv writer batch :separator separator-char)))
  output-filepath)

(defn write-to-file-multiple
  [{:keys [output-filepath write-size] :as input-map} data]
  (->> data
       (partition-all (or write-size batch-size-write))
       (reduce
        (fn [{:keys [file-version] :as file-map} batch]
          (let [output-filepath (str output-filepath "-" (p/pad-left (str file-version) 3 "0"))
                input-map-adj (merge input-map {:output-filepath output-filepath})]
            (-> file-map
                (update-in [:files] #(conj % (write-to-file input-map-adj batch)))
                (update-in [:file-version] inc))))
        {:files [] :file-version 1})
       :files))