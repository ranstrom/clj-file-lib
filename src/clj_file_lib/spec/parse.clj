(ns clj-file-lib.spec.parse
  (:require [clj-file-lib.parse :as p]
            [clojure.spec.alpha :as s]))

(s/def ::uri (s/with-gen
               #(or (uri? %) (string? %))
               #(s/gen uri?)))

(s/fdef p/parse-url
        :args (s/cat :u ::uri)
        :ret map?)

(s/fdef p/parse-uri
        :args (s/cat :u ::uri)
        :ret map?)

(s/fdef p/parse-transient
        :args (s/cat :u uri?)
        :ret map?)