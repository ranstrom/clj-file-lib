(ns clj-file-lib.core
  (:require [cheshire.core :as c]
            [clj-file-lib.parse :as p]
            [clj-file-lib.providers.azure-blob :as azure-blob]
            [clj-file-lib.providers.azure-data-lake :as azure-data-lake]
            [clj-file-lib.providers.local :as fh-local]
            [clj-file-lib.providers.ftp :as fh-ftp]
            [clj-file-lib.providers.sftp :as fh-sftp]
            [clj-file-lib.providers.http :as fh-http]
            [clj-file-lib.providers.oci :as fh-oracle]
            [clojure.core.async :refer [>! <! go-loop chan close! <!!]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [digest :as digest]
            [taoensso.timbre :as log])
  (:import (java.io FileInputStream)
           (java.util.zip GZIPInputStream)))

; Count lines borrowed from:
; https://gist.github.com/atroche/08b966664489f80e87a145cef6775b07

(def ^:private one-meg (* 1024 1024))

(defn- count-newlines [^bytes barray]
  (let [num-bytes (alength barray)]
    (loop [i 0
           newlines 0]
      (if (>= i num-bytes)
        newlines
        (if (= 10 (aget ^bytes barray i))
          (recur (inc i)
                 (inc newlines))
          (recur (inc i)
                 newlines))))))

(defn- count-file-rows-async
  "Performance file counter over large data-sets"
  [filepath gzip]
  (with-open [file-stream (-> filepath FileInputStream. (#(if gzip (GZIPInputStream. %) %)))]
    (let [channel (chan 500)
          counters (for [_ (range 4)]
                     (go-loop [newline-count 0]
                       (let [barray (<! channel)]
                         (if (nil? barray)
                           newline-count
                           (recur (+ newline-count (count-newlines barray)))))))]
      (go-loop []
        (let [barray (byte-array one-meg)
              bytes-read (.read file-stream barray)]
          (>! channel barray)
          (if (pos? bytes-read) (recur) (close! channel))))
      (reduce + (map <!! counters)))))

(defn- get-uri-scheme
  [filepath]
  (let [uri (-> (p/parse-uri filepath) :scheme p/lower-case)]
    (if (or (nil? uri) (some #(= uri %) ["file" "c"])) "local" uri)))

(def file-registry
  {:azure-blob
   {:exists?  azure-blob/file-exists?
    :list     azure-blob/list-files
    :download azure-blob/download-file
    :remove   azure-blob/remove-file
    :upload   azure-blob/upload-file}
   :azure-data-lake
   {:exists?  azure-data-lake/file-exists?
    :list     azure-data-lake/list-files
    :download azure-data-lake/download-file
    :remove   azure-data-lake/remove-file
    :upload   azure-data-lake/upload-file}
   :ftp   {:exists?  fh-ftp/file-exists?
           :list     fh-ftp/list-files
           :download fh-ftp/download-file
           :remove   fh-ftp/remove-file
           :upload   fh-ftp/upload-file}
   :https {:exists?  fh-http/file-exists?
           :list     fh-http/list-files
           :download fh-http/download-file
           :remove   fh-http/remove-file
           :upload   fh-http/upload-file}
   :http  {:exists?  fh-http/file-exists?
           :list     fh-http/list-files
           :download fh-http/download-file
           :remove   fh-http/remove-file
           :upload   fh-http/upload-file}
   :local {:exists?  fh-local/file-exists?
           :list     fh-local/list-files
           :download fh-local/download-file
           :remove   fh-local/remove-file
           :upload   fh-local/upload-file}
   :oci   {:exists?  fh-oracle/file-exists?
           :list     fh-oracle/list-files
           :download fh-oracle/download-file
           :remove   fh-oracle/remove-file
           :upload   fh-oracle/upload-file}
   :sftp  {:exists?  fh-sftp/file-exists?
           :list     fh-sftp/list-files
           :download fh-sftp/download-file
           :remove   fh-sftp/remove-file
           :upload   fh-sftp/upload-file}})

(defn- exec
  [uri k inputs]
  (if-let [rf (-> file-registry uri k)]
    (apply rf inputs)
    (throw (Exception. (str "Registry func not found for uri " uri " and key " k)))))

(defprotocol FileProtocol
  (count-rows [this options])
  (download-file [this conn-props])
  (file->map [this conn-props])
  (file-exists? [this conn-props])
  (file-exists-local? [this])
  (checksum [this])
  (list-files [this conn-props])
  (list-files-local [this])
  (map->json-file [this data])
  (remove-file [this conn-props])
  (remove-file-local [this])
  ;(update-file [this k v])
  (upload-file [this conn-props]))

(defrecord File [filepath remote-path local-path remove-uploaded uri]
  FileProtocol
  (count-rows [_ {:keys [gzip]}]
    (count-file-rows-async filepath gzip))
  (download-file [this conn-props]
    (exec uri :download [this conn-props]))
  (file->map [this conn-props]
    (when (file-exists? this conn-props)
      (let [type (-> filepath p/get-file-extension string/lower-case keyword)]
        (case type
          :manifest (c/parse-string (slurp filepath) true)
          :json (c/parse-string (slurp filepath) true)
          :edn (edn/read-string (slurp filepath))
          :else (throw (Exception. (str "File type " type " unsupported for " filepath)))))))
  (file-exists? [this conn-props]
    (exec uri :exists? [this conn-props]))
  (file-exists-local? [this] (file-exists? this {}))
  (checksum [_]
    (-> filepath io/as-file digest/md5))
  (list-files [this conn-props]
    (exec uri :list [this conn-props]))
  (list-files-local [this] (list-files this {}))
  (map->json-file [_ data]
    (try
      (log/debug "Writing to path: " filepath ", data:\n" data)
      (->> filepath io/writer (c/generate-stream data))
      filepath
      (catch Exception e
        (throw (Exception. (str "Error writing map to json: " e))))))
  (remove-file [this conn-props]
    (exec uri :remove [this conn-props]))
  (remove-file-local [this] (remove-file this {}))
  ;(update-file [this k v] (update this k v))
  (upload-file [this conn-props]
    (let [ret (exec uri :upload [this conn-props])]
      (when (:remove-uploaded this)
        (remove-file this conn-props))
      ret)))

(defn file
  [{:keys [filepath remote-path local-path remove-uploaded]}]
  (let [uri (-> filepath get-uri-scheme keyword)]
    (when-not (contains? file-registry uri)
      (throw (Exception. (str "Unsupported URI: " uri))))
    (map->File
     {:filepath        filepath
      :local-path      local-path
      :remote-path     remote-path
      :remove-uploaded remove-uploaded
      :uri             uri})))

(defn create-directory
  [filepath]
  (try
    (io/make-parents filepath)
    filepath
    (catch Exception e
      (throw (Exception. (str "Failed creating directories for path: " filepath ", " e))))))
