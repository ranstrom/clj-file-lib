(ns clj-file-lib.providers.local
  (:require [clj-file-lib.parse :as p]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]))

(defn file-exists?
  [{:keys [filepath]} _]
  (.exists (io/as-file filepath)))

(defn list-files
  [{:keys [filepath]} _]
  (let [files-in-dir (-> filepath p/get-dir-from-path io/file .listFiles)]
    (->> (p/get-file-pattern filepath)
         (#(p/get-file-matches % files-in-dir))
         (map p/update-win-style-paths))))

(defn download-file
  [{:keys [remote-path local-path]} _]
  (log/debug (str "Remote file " remote-path " is local. Copying file if needed to: " local-path))
  (when (not= remote-path local-path)
    (io/make-parents local-path)
    (io/copy (io/file remote-path) (io/file local-path)))
  local-path)

(defn upload-file
  [{:keys [local-path remote-path]} _]
  (log/debug (str "Remote file " remote-path " is local. Copying file if needed."))
  (when (not= local-path remote-path)
    (io/copy (io/file local-path) (io/file remote-path)))
  remote-path)

(defn remove-file
  [{:keys [filepath]} _]
  (io/delete-file filepath))