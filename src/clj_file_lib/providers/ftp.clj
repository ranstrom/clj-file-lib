(ns clj-file-lib.providers.ftp
  (:require [clj-file-lib.parse :as p]
            [miner.ftp :as ftp]))

(defn- ftp-connect
  [conn-props remote-path input-func]
  (try
    (let [remote-dir (-> remote-path
                         (p/replace-conn-props-in-path conn-props)
                         p/get-dir-from-path)]
      (ftp/with-ftp
        [_ remote-dir
         ; possible this only works on transfers
         :file-type :binary]
        (input-func)))
    (catch Exception e
      (throw (Exception. (str "Error occurred during FTP operation: " e))))))

(defn file-exists?
  [{:keys [filepath]} conn-props]
  (ftp-connect
   conn-props
   filepath
   (partial
    (fn [fp client]
      (let [filename (p/get-filename-from-path fp)]
        (contains? (set (ftp/client-file-names client)) filename)))
    filepath)))

(defn list-files
  [{:keys [filepath]} conn-props]
  (ftp-connect
   conn-props
   filepath
   (partial
    (fn [fp client]
      (let [uri-path (:path (p/parse-uri fp))
            file-pattern (p/get-file-pattern
                          (p/get-filename-from-path uri-path))]
        (->> (ftp/client-file-names client)
             (p/get-file-matches file-pattern)
             (p/append-dir-to-files (p/get-dir-from-path fp)))))
    filepath)))

(defn download-file
  [{:keys [local-path remote-path]} conn-props]
  (ftp-connect
   conn-props
   remote-path
   (partial
    (fn [rp lp client]
      (let [remote-filename (p/get-filename-from-path rp)]
        (ftp/client-get client remote-filename lp)
        lp))
    remote-path
    local-path)))

(defn upload-file
  [{:keys [local-path remote-path]} conn-props]
  (ftp-connect
   conn-props
   remote-path
   (partial
    (fn [lp rp client]
      (let [remote-filename (p/get-filename-from-path rp)]
        (ftp/client-put client lp remote-filename)
        rp))
    local-path
    remote-path)))

(defn remove-file
  [{:keys [filepath]} conn-props]
  (ftp-connect
   conn-props
   filepath
   (partial
    (fn [fp client]
      (ftp/client-delete client (p/get-filename-from-path fp)))
    filepath)))