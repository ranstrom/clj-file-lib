(ns clj-file-lib.providers.http
  (:require [clj-http.client :as client]
            [clojure.java.io :as io]
            [taoensso.timbre :as log])
  (:import (java.io File)))

;(def valid-auth-types ["none" "basic" "digest" "ntlm" "oauth"])

(defn- get-auth-from-properties
  [conn-props]
  (let [auth_type (-> conn-props :auth_type keyword)]
    (case auth_type
      :none nil
      :basic {:basic-auth [(:user conn-props)
                           (:pswd conn-props)]}
      :digest {:digest-auth [(:user conn-props)
                             (:pswd conn-props)]}
      :ntlm {:ntlm-auth [(:user conn-props)
                         (:pswd conn-props)
                         (:host conn-props)
                         (:domain conn-props)]}
      :oauth {:oauth-token (:token conn-props)}
      (throw (Exception. (str "Invalid auth_type: " auth_type))))))

(defn file-exists?
  [{:keys [filepath]} conn-props]
  (try
    (client/get filepath (get-auth-from-properties conn-props))
    true
    (catch Exception _
      false)))

(defn list-files
  [{:keys [filepath]} conn-props]
  (log/warn "Listing files not supported for http/https. Returning filepath if file exists.")
  (when (file-exists? {:filepath filepath} conn-props)
    [filepath]))

(defn download-file
  [{:keys [local-path remote-path]} conn-props]
  (io/copy (:body (client/get remote-path (merge {:as :stream}
                                                 (get-auth-from-properties conn-props))))
           (File. local-path))
  local-path)

(defn upload-file
  [{:keys [remote-path]} _]
  (log/warn "Uploading files to http/https not supported")
  remote-path)

(defn remove-file [] (log/warn "Removing files not supported for http/https."))