(ns clj-file-lib.providers.sftp
  (:require [clj-ssh.ssh :as ssh]
            [clj-file-lib.parse :as p]
            [taoensso.timbre :as log]))

(defn- ls-entry>name
  [input]
  (vec
   (map #(.getFileName %) input)))

(defn- get-connection-info
  [{:keys [user pswd port]}]
  (merge {:strict-host-key-checking :no}
         (when user {:username user})
         (when pswd {:password pswd})
         (when port {:port port})))

(defn- sftp-connect
  [{:keys [host ssh-key] :as conn-props} input-func]
  (let [agent (ssh/ssh-agent {:use-system-ssh-agent false})]
    (when ssh-key
      (ssh/add-identity agent {:private-key-path ssh-key}))
    (let [session (ssh/session agent host (get-connection-info conn-props))]
      (ssh/with-connection
        session
        (input-func)))))

; COMMON


(defn file-exists?
  [{:keys [filepath]} conn-props]
  (sftp-connect
   conn-props
   (partial
    (fn [fp channel]
      (try
        (ssh/sftp channel {} :ls (:path (p/parse-uri fp)))
        true
        (catch Exception e
          (log/debug (str "File doesn't exist: " e))
          false)))
    filepath)))

(defn list-files
  [{:keys [filepath]} conn-props]
  (sftp-connect
   conn-props
   (partial
    (fn [fp channel]
      (let [uri-path (:path (p/parse-uri fp))
            file-pattern (p/get-file-pattern
                          (p/get-filename-from-path uri-path))
            path (p/get-dir-from-path uri-path)]
        (->> (ssh/sftp channel {} :ls path)
             (ls-entry>name)
             (p/get-file-matches file-pattern)
             (p/append-dir-to-files (p/get-dir-from-path fp)))))
    filepath)))

(defn download-file
  [{:keys [local-path remote-path]} conn-props]
  (sftp-connect
   conn-props
   (partial
    (fn [rp lp channel]
      (ssh/sftp channel {} :get (:path (p/parse-uri rp)) lp)
      lp)
    remote-path
    local-path)))

(defn upload-file
  [{:keys [local-path remote-path]} conn-props]
  (sftp-connect
   conn-props
   (partial
    (fn [lp rp channel]
      (ssh/sftp channel {} :put lp (:path (p/parse-uri rp)))
      rp)
    local-path
    remote-path)))

(defn remove-file
  [{:keys [filepath]} conn-props]
  (sftp-connect
   conn-props
   (partial
    (fn [fp channel]
      (ssh/sftp channel {} :put fp (:path (p/parse-uri fp))))
    filepath)))