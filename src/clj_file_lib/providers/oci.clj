(ns clj-file-lib.providers.oci
  (:require [clojure.string :as string]
            [clj-file-lib.parse :as p])
  (:import (java.util HashMap)))

;(defn- OCIFileHandler (Class/forName "com.something.OCIFileHandler"))

(defn- oci-download [_]
  ;(doto (new OCIFileHandler)
  ;                                         (.downloadFile input-map))
)

(defn- oci-upload [_]
  ;(doto (new OCIFileHandler)
  ;                                       (.uploadFile input-map))
)

(defn- parse-uri-for-object-storage
  [uri-string]
  (let [uri (p/parse-uri uri-string)]
    {:bucket    (as->
                 (:path uri) p
                  (subs p (+ (string/index-of p "/b/") 3))
                  (string/split p #"\/")
                  (get p 0))
     :filename  (last (string/split (:path uri) #"\/"))
     :namespace (as->
                 (:path uri) p
                  (subs p (+ (string/index-of p "/n/") 3))
                  (string/split p #"\/")
                  (get p 0))
     :object    (as->
                 (:path uri) p
                  (subs p (+ (string/index-of p "/o/") 3)))}))

(defn file-exists?
  [_ _])

(defn list-files
  [_ _])

(defn download-file
  [{:keys [remote-path local-path]} _]
  (try
    (let [transient-map (parse-uri-for-object-storage remote-path)
          input-map (HashMap. {"bucket"    (:bucket transient-map)
                               "object"    (:object transient-map)
                               "namespace" (:namespace transient-map)
                               "localFile" local-path})]
      (oci-download input-map)
      local-path)
    (catch Exception e
      (throw (Exception. (str "Error downloading Oracle ObjectStorage file: " e))))))

(defn upload-file
  [{:keys [local-path remote-path]} conn-props]
  (try
    (let [transient-map (parse-uri-for-object-storage remote-path)
          input-map (HashMap. {"bucket"          (:bucket transient-map)
                               "object"          (:object transient-map)
                               "namespace"       (:namespace conn-props)
                               "localFile"       local-path
                               "contentType"     "text/csv" ; application/gzip
                               "contentEncoding" "gzip"
                               "contentLanguage" "en-US"})]
      (oci-upload input-map))
    remote-path
    (catch Exception e
      (throw (Exception. (str "Error uploading Oracle ObjectStorage file: " e))))))

(defn remove-file
  [_ _])