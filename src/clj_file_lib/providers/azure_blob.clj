(ns clj-file-lib.providers.azure-blob
  (:import (com.azure.storage.blob BlobClientBuilder BlobClient)))

;(defn- create-blob-service-client [{:keys [endpoint sas-token]}]
;  (-> (BlobServiceClientBuilder.)
;      (.endpoint endpoint)
;      (.sasToken sas-token)
;      (.buildClient)))
;
;(defn- create-blob-container-client [{:keys [container-name] :as input}]
;  (let [^BlobServiceClient blob-service-client (create-blob-service-client input)]
;    (.getBlobContainerClient blob-service-client container-name)))
;
;(defn- create-blob-client [input blob-path]
;  (let [^BlobContainerClient blob-container-client (create-blob-container-client input)]
;    (.getBlobClient blob-container-client blob-path)))

(defn- create-blob-client [{:keys [endpoint sas-token container-name]} blob-path]
  (-> (BlobClientBuilder.)
      (.endpoint endpoint)
      (.sasToken sas-token)
      (.containerName container-name)
      (.blobName blob-path)
      (.buildClient)))

(defn file-exists?
  [{:keys [filepath]} conn-props]
  (let [^BlobClient client (create-blob-client conn-props filepath)]
    (.exists client)))

(defn list-files
  [{:keys [filepath] :as input-map} conn-props]
  (when (file-exists? input-map conn-props)
    [filepath]))

(defn download-file
  [{:keys [local-path remote-path]} conn-props]
  (let [^BlobClient client (create-blob-client conn-props remote-path)]
    (.downloadToFile client local-path)))

(defn upload-file
  [{:keys [local-path remote-path]} conn-props]
  (let [^BlobClient client (create-blob-client conn-props remote-path)]
    (.uploadFromFile client local-path)))

(defn remove-file
  [{:keys [filepath]} conn-props]
  (let [^BlobClient client (create-blob-client conn-props filepath)]
    (.delete client)))