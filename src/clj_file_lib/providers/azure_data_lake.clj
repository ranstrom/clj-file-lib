(ns clj-file-lib.providers.azure-data-lake
  (:import (com.azure.storage.file.datalake DataLakeFileClient DataLakePathClientBuilder)))

;(defn- create-data-lake-service-client [{:keys [endpoint sas-token]}]
;  (-> (DataLakeServiceClientBuilder.)
;      (.endpoint endpoint)
;      (.sasToken sas-token)
;      (.buildClient)))
;
;(defn- create-data-lake-file-system-client [{:keys [file-system] :as input}]
;  (let [^DataLakeServiceClient data-lake-service-client (create-data-lake-service-client input)]
;    (.getFileSystemClient data-lake-service-client file-system)))
;
;(defn- create-data-lake-file-client [conn-props file-path]
;  (let [^DataLakeFileSystemClient data-lake-file-system-client (create-data-lake-file-system-client conn-props)]
;    (.getDirectoryClient data-lake-file-system-client file-path)))

(defn- create-data-lake-file-client [{:keys [endpoint sas-token file-system]} blob-path]
  (-> (DataLakePathClientBuilder.)
      (.endpoint endpoint)
      (.sasToken sas-token)
      (.fileSystemName file-system)
      (.pathName blob-path)
      (.buildFileClient)))

;(defn- create-data-lake-directory-client [{:keys [] :as conn-props} blob-path]
;  (let [^DataLakeFileSystemClient data-lake-file-system-client (create-data-lake-file-system-client conn-props)]
;    (.getFileClient data-lake-file-system-client blob-path)))

(defn file-exists?
  [{:keys [filepath]} conn-props]
  (let [^DataLakeFileClient client (create-data-lake-file-client conn-props filepath)]
    (.exists client)))

(defn list-files
  [{:keys [filepath] :as input-map} conn-props]
  (when (file-exists? input-map conn-props)
    [filepath]))

(defn download-file
  [{:keys [local-path remote-path]} conn-props]
  (let [^DataLakeFileClient client (create-data-lake-file-client conn-props remote-path)]
    (.readToFile client local-path)))

(defn upload-file
  [{:keys [local-path remote-path]} conn-props]
  (let [^DataLakeFileClient client (create-data-lake-file-client conn-props remote-path)]
    (.uploadFromFile client local-path)))

(defn remove-file
  [{:keys [filepath]} conn-props]
  (let [^DataLakeFileClient client (create-data-lake-file-client conn-props filepath)]
    (.delete client)))