(ns clj-file-lib.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [clj-file-lib.core :as fh]
            ; [clj-file-lib.providers.ftp :as fh-ftp]
            ; [clj-file-lib.providers.sftp :as fh-sftp]
            ; [clj-file-lib.providers.http :as fh-http]
            ; [clj-file-lib.providers.oci :as fh-oci]
            ; [clj-file-lib.providers.local :as fh-local]
))

(deftest test-get-file-checksum
  (testing "Getting file checksum"
    (is (fh/checksum (fh/file {:filepath "./deps.edn"})))))

;(deftest test-shared-file-functions
;  (testing "file-exists?"
;    (with-redefs [fh-ftp/file-exists? (constantly "ftp-ret")
;                  fh-http/file-exists? (constantly "http-ret")
;                  fh-sftp/file-exists? (constantly "sftp-ret")
;                  fh-oci/file-exists? (constantly "oci-ret")]
;      (is (= (fh/file-exists? (fh/file {:filepath "ftp://host:80/some/file/path.txt"}) {}) "ftp-ret"))
;      (is (= (fh/file-exists? (fh/file {:filepath "https://host:80/some/file/path.txt"}) {}) "http-ret"))
;      (is (= (fh/file-exists? (fh/file {:filepath "http://host:80/some/file/path.txt"}) {}) "http-ret"))
;      (is (= (fh/file-exists? (fh/file {:filepath "sftp://host:80/some/file/path.txt"}) {}) "sftp-ret"))
;      (is (= (fh/file-exists? (fh/file {:filepath "oci://host:80/some/file/path.txt"}) {}) "oci-ret"))
;      (is (not (fh/file-exists? (fh/file {:filepath "/some/random/path.txt"}) {})))
;      (is (thrown? Exception (fh/file-exists? (fh/file {:filepath "s3://unsupported/today"}) {})))))
;  (testing "list-files"
;    (with-redefs [fh-ftp/list-files (constantly "ftp-ret")
;                  fh-http/list-files (constantly "http-ret")
;                  fh-sftp/list-files (constantly "sftp-ret")
;                  fh-oci/list-files (constantly "oci-ret")
;                  fh-local/list-files (constantly "local-ret")]
;      (is (= (fh/list-files (fh/file {:filepath "ftp://host:80/some/file/path.txt"}) {}) "ftp-ret"))
;      (is (= (fh/list-files (fh/file {:filepath "https://host:80/some/file/path.txt"}) {}) "http-ret"))
;      (is (= (fh/list-files (fh/file {:filepath "http://host:80/some/file/path.txt"}) {}) "http-ret"))
;      (is (= (fh/list-files (fh/file {:filepath "sftp://host:80/some/file/path.txt"}) {}) "sftp-ret"))
;      (is (= (fh/list-files (fh/file {:filepath "oci://host:80/some/file/path.txt"}) {}) "oci-ret"))
;      (is (= (fh/list-files (fh/file {:filepath "/some/random/path.txt"}) {}) "local-ret"))
;      (is (thrown? Exception (fh/list-files (fh/file {:filepath "s3://unsupported/today"}) {})))))
;  (testing "download-file"
;    (with-redefs [fh-ftp/download-file  (constantly "ftp-ret")
;                  fh-http/download-file  (constantly "http-ret")
;                  fh-sftp/download-file  (constantly "sftp-ret")
;                  fh-oci/download-file  (constantly "oci-ret")
;                  fh-local/download-file  (constantly "local-ret")]
;      (is (= (fh/download-file  (fh/file {:remote-path "ftp://host:80/some/file/path.txt"}) {}) "ftp-ret"))
;      (is (= (fh/download-file  (fh/file {:remote-path "https://host:80/some/file/path.txt"}) {}) "http-ret"))
;      (is (= (fh/download-file  (fh/file {:remote-path "http://host:80/some/file/path.txt"}) {}) "http-ret"))
;      (is (= (fh/download-file  (fh/file {:remote-path "sftp://host:80/some/file/path.txt"}) {}) "sftp-ret"))
;      (is (= (fh/download-file  (fh/file {:remote-path "oci://host:80/some/file/path.txt"}) {}) "oci-ret"))
;      (is (= (fh/download-file  (fh/file {:remote-path "/some/random/path.txt"}) {}) "local-ret"))
;      (is (thrown? Exception (fh/download-file (fh/file {:remote-path "s3://unsupported/today"}) {})))))
;  (testing "remove-file"
;    (with-redefs [fh-ftp/remove-file (constantly "ftp-ret")
;                  fh-http/remove-file (constantly "http-ret")
;                  fh-sftp/remove-file (constantly "sftp-ret")
;                  fh-oci/remove-file (constantly "oci-ret")
;                  fh-local/remove-file (constantly "local-ret")]
;      (is (= (fh/remove-file (fh/file {:filepath "ftp://host:80/some/file/path.txt"}) {}) "ftp-ret"))
;      (is (= (fh/remove-file (fh/file {:filepath "https://host:80/some/file/path.txt"}) {}) "http-ret"))
;      (is (= (fh/remove-file (fh/file {:filepath "http://host:80/some/file/path.txt"}) {}) "http-ret"))
;      (is (= (fh/remove-file (fh/file {:filepath "sftp://host:80/some/file/path.txt"}) {}) "sftp-ret"))
;      (is (= (fh/remove-file (fh/file {:filepath "oci://host:80/some/file/path.txt"}) {}) "oci-ret"))
;      (is (= (fh/remove-file (fh/file {:filepath "/some/random/path.txt"}) {}) "local-ret"))
;      (is (thrown? Exception (fh/remove-file (fh/file {:filepath "s3://unsupported/today"}) {})))))
;  (testing "upload-file"
;    (with-redefs [fh-ftp/upload-file  (constantly "ftp-ret")
;                  fh-http/upload-file  (constantly "http-ret")
;                  fh-sftp/upload-file  (constantly "sftp-ret")
;                  fh-oci/upload-file  (constantly "oci-ret")
;                  fh-local/upload-file  (constantly "local-ret")]
;      (is (= (fh/upload-file  (fh/file {:remote-path "ftp://host:80/some/file/path.txt"}) {}) "ftp://host:80/some/file/path.txt"))
;      (is (= (fh/upload-file  (fh/file {:remote-path "https://host:80/some/file/path.txt"}) {}) "https://host:80/some/file/path.txt"))
;      (is (= (fh/upload-file  (fh/file {:remote-path "http://host:80/some/file/path.txt"}) {}) "http://host:80/some/file/path.txt"))
;      (is (= (fh/upload-file  (fh/file {:remote-path "sftp://host:80/some/file/path.txt"}) {}) "sftp://host:80/some/file/path.txt"))
;      (is (= (fh/upload-file  (fh/file {:remote-path "oci://host:80/some/file/path.txt"}) {}) "oci://host:80/some/file/path.txt"))
;      (is (= (fh/upload-file  (fh/file {:remote-path "/some/random/path.txt"}) {}) "/some/random/path.txt"))
;      (is (thrown? Exception (fh/upload-file (fh/file {:remote-path "s3://unsupported/today"}) {}))))))
