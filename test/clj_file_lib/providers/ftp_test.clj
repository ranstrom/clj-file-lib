(ns clj-file-lib.providers.ftp-test
  (:require [clojure.test :refer [deftest is]]
            [miner.ftp :as ftp]
            [clj-file-lib.providers.ftp :as fh-ftp]))

(deftest test-ftp-connect
  (with-redefs [fh-ftp/ftp-connect (fn [_ _ func] (func {}))]
    (with-redefs [ftp/client-file-names (constantly
                                         (vector "not-file.txt"
                                                 "file.txt"))]
      (is (fh-ftp/file-exists? {:filepath "/some/path/file.txt"} {})))
    (with-redefs [ftp/client-file-names (constantly
                                         (vector "not-file.txt"))]
      (is (not (fh-ftp/file-exists? {:filepath "/some/path/file.txt"} {}))))

    (with-redefs [ftp/client-file-names (constantly
                                         (vector "file1.csv"
                                                 "file2.csv"
                                                 "file3.txt"
                                                 "nfile4.csv"))]
      (is (= 2 (count (fh-ftp/list-files {:filepath "sftp://host:port/some/path/file*.csv"} {})))))

    (with-redefs [ftp/client-get (constantly true)]
      (is (= "/local/path"
             (fh-ftp/download-file {:local-path  "/local/path"
                                    :remote-path "/remote/path"} {}))))

    (with-redefs [ftp/client-put (constantly true)]
      (is (= "/remote/path"
             (fh-ftp/upload-file {:local-path  "/local/path"
                                  :remote-path "/remote/path"} {}))))

    ;(with-redefs [ftp/client-delete (constantly true)]
    ;  (is (fh-ftp/remove-file "/some/path" {})))
    ))