(ns clj-file-lib.providers.sftp-test
  (:require [clj-file-lib.providers.sftp :as fh-sftp]
            [clojure.test :refer [deftest is testing]]
            [clj-ssh.ssh :as ssh]))

(deftest test-get-connection-info
  (testing "get-connection-info"
    (let [func #'fh-sftp/get-connection-info]
      (is (= (func {:user "elrond" :pswd "no-orcs" :port 22})
             {:username                 "elrond" :password "no-orcs" :port 22
              :strict-host-key-checking :no}))
      (is (= (func {})
             {:strict-host-key-checking :no})))))

(deftest test-sftp-connect
  (with-redefs [fh-sftp/sftp-connect (fn [_ func] (func {}))]
    (with-redefs [ssh/sftp (constantly true)]
      (is (fh-sftp/file-exists? {:filepath "/some/path"} {})))
    (with-redefs [ssh/sftp (fn []
                             (throw (Exception. (str "Not found"))))]
      (is (not (fh-sftp/file-exists? {:filepath "/some/path"} {}))))

    (with-redefs [ssh/sftp (constantly
                            (vector "file1.csv"
                                    "file2.csv"
                                    "file3.txt"
                                    "nfile4.csv"))
                  fh-sftp/ls-entry>name (fn [x] x)]
      (is (= 2 (count (fh-sftp/list-files {:filepath "sftp://host:port/some/path/file*.csv"} {})))))

    (with-redefs [ssh/sftp (constantly true)]
      (is (= "/local/path"
             (fh-sftp/download-file {:local-path  "/local/path"
                                     :remote-path "/remote/path"} {}))))

    (with-redefs [ssh/sftp (constantly true)]
      (is (= "/remote/path"
             (fh-sftp/upload-file {:local-path  "/local/path"
                                   :remote-path "/remote/path"} {}))))

    (with-redefs [ssh/sftp (constantly true)]
      (is (fh-sftp/remove-file {:filepath "/some/path"} {})))))