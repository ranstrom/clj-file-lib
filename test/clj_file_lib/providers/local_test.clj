(ns clj-file-lib.providers.local-test
  (:require [clojure.test :refer :all]
            [clj-file-lib.providers.local :as fh-local]
            [clj-file-lib.fixtures :as fx])
  (:import (java.util UUID)))

(defn uuid [] (str (UUID/randomUUID)))

(def file-dir-atom (atom (str "./" (uuid))))

(defn- st-file-fixture [f]
  (fx/st-fixture f {:create-dirs [@file-dir-atom]}))

(use-fixtures :once st-file-fixture)

(deftest test-file-exists?
  (testing "non-existent properties files are bad"
    (let [file-exists (fh-local/file-exists? {:filepath "/some/random/path.json"} {})]
      (is (false? file-exists)))))

(deftest test-list-files
  (testing "listing files"
    (fx/create-files [(str @file-dir-atom "/file1.txt")
                      (str @file-dir-atom "/file2.txt")])
    (is (= 2 (count (fh-local/list-files {:filepath (str @file-dir-atom "/file*.txt")} {}))))))
