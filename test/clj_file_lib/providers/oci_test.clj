(ns clj-file-lib.providers.oci-test
  (:require [clojure.test :refer :all]
            [clj-file-lib.providers.oci :as fh-oci]))

(def ^:private mock-conn-props {:config_path "~/.oci/config"
                                :namespace   "my-namespace"})

(def ^:private mock-file-map {:remote-path "oci://bucket/path/to/file.csv"
                              :local-path  "/some/local/file.csv"})

(deftest test-parse-uri-for-object-storage
  (testing "parse uri for object storage"
    (let [func #'fh-oci/parse-uri-for-object-storage]
      (is (= (func "https://objectstorage.us-ashburn-1.oraclecloud.com/n/gandalfs/b/wizardry/o/tf/my.tfstate")
             {:bucket "wizardry"
              :filename "my.tfstate"
              :namespace "gandalfs"
              :object "tf/my.tfstate"})))))

;(deftest test-oci-file-processes
;  (testing "download-file"
;    (with-redefs [fh-oci/oci-download (constantly "ret")]
;      (is (= "/some/local/file.csv"
;             (fh-oci/download-file mock-file-map
;                                   mock-conn-props))))
;    (with-redefs [fh-oci/oci-download (fn [input] (throw (Exception. "err")))]
;      (is (thrown? Exception (fh-oci/download-file mock-file-map
;                                                   mock-conn-props)))))
;  (testing "upload-file"
;    (with-redefs [fh-oci/oci-upload (constantly "ret")]
;      (is (= "oci://bucket/path/to/file.csv"
;             (fh-oci/upload-file mock-file-map
;                                 mock-conn-props))))
;    (with-redefs [fh-oci/oci-upload (fn [input] (throw (Exception. "err")))]
;      (is (thrown? Exception (fh-oci/upload-file mock-file-map
;                                                 mock-conn-props))))))